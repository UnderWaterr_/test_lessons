package com.tests.config;

import com.tests.models.AccountData;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public final class Settings {
    public static String file = "src/main/java/resources/Settings.xml";

    private static final Properties properties;
    private static final AccountData accountData;

    static {
        try (FileInputStream fis = new FileInputStream(file)) {
            properties = new Properties();
            properties.loadFromXML(fis);
            accountData = new AccountData(properties.getProperty("email"),
                    properties.getProperty("password"),
                    properties.getProperty("name"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static AccountData getAccountDataFromSettings() {
        return accountData;
    }
}
