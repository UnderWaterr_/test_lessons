package com.tests;

import com.tests.helpers.LoginHelper;
import com.tests.helpers.MessageHelper;
import com.tests.helpers.NavigationHelper;
import com.tests.helpers.RemoveMessageHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class ApplicationManager {
    private static final String CHROME_DRIVER_URL =
            ".\\chrome.driver\\chromedriver.exe";
    private static final String CHROME_DRIVER_NAME = "webdriver.chrome.driver";

    public static final String BASE_URL = "https://grouple.co";

    private static final ThreadLocal<ApplicationManager> app = new ThreadLocal<>();
    public int counter = 0;
    private final WebDriver driver;
    private final NavigationHelper navigationHelper;
    private final LoginHelper loginHelper;
    private final MessageHelper messageHelper;
    private final RemoveMessageHelper removeMessageHelper;

    private ApplicationManager() {
        System.setProperty(CHROME_DRIVER_NAME, CHROME_DRIVER_URL);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));

        navigationHelper = new NavigationHelper(this);
        loginHelper = new LoginHelper(this);
        messageHelper = new MessageHelper(this);
        removeMessageHelper = new RemoveMessageHelper(this);
        DestructorUtil.addManagerDestructor(this);
    }

    public static ApplicationManager getInstance() {
        if (app.get() == null) {
            ApplicationManager newInstance = new ApplicationManager();
            newInstance.loginHelper.openLoginPage();
            app.set(newInstance);
        }
        return app.get();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    public MessageHelper getMessageHelper() {
        return messageHelper;
    }

    public RemoveMessageHelper getDeleteMessageHelper() {
        return removeMessageHelper;
    }

}
