package com.tests.helpers;

import com.tests.ApplicationManager;
import com.tests.base.HelperBase;
import com.tests.models.MessageData;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class MessageHelper extends HelperBase {
    private static final String CHAT_PAGE_URL = "/chat/index";
    private MessageData messageData;
    private String messageText = "";
    private final String messagePageUrl;

    public MessageHelper(ApplicationManager applicationManager) {
        super(applicationManager);
        messagePageUrl = ApplicationManager.BASE_URL + CHAT_PAGE_URL;
    }

    public void openChatPage() {
        driver.get(messagePageUrl);
    }

    public void sendMessage() throws InterruptedException {
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[3]/a")).click();
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[2]/div/form/div/div/div")).click();
        driver.findElement(By.xpath("//input[@type='select-multiple']")).sendKeys(messageData.getRecipient());
        Thread.sleep(1000);
        driver.findElement(By.xpath("//input[@type='select-multiple']")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[2]/div/form/div[2]/div/div[2]/div")).click();
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[2]/div/form/div[2]/div/div[2]/div"))
                .sendKeys(messageData.getText());
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[2]/div/form/input")).click();
    }

    public String getCurrentMessage() {
        return driver.findElement(By.xpath("//div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]")).getText();
    }

    public String getMessageText() {
        return messageText;
    }

    public String getMessageRecipient() {
        return messageData.getRecipient();
    }

    public void setMessage(MessageData message) {
        messageData = message;
        if (!messageText.equals("")) {
            messageText += "\n";
        }
        messageText += message.getText();
    }
}
