package com.tests.helpers;

import com.tests.ApplicationManager;
import com.tests.base.HelperBase;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class RemoveMessageHelper extends HelperBase {

    private boolean acceptNextAlert = true;

    public RemoveMessageHelper(ApplicationManager applicationManager) {
        super(applicationManager);
    }

    public void deleteMessage() {
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[3]/div[2]/ul/li[2]/a")).click();
        closeAlertAndGetItsText();
    }

    private void closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
        } finally {
            acceptNextAlert = true;
        }
    }

    public String getMessageErrorText() throws InterruptedException {
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[3]/div[2]/div[2]/div/div/input")).clear();
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[3]/div[2]/div[2]/div/div/input"))
                .sendKeys(applicationManager.getMessageHelper().getMessageRecipient());
        Thread.sleep(1000);
        driver.findElement(By.xpath("//div[@id='mangaBox']/div[3]/div[2]/div[2]/div/div/input")).sendKeys(Keys.ENTER);
        return driver.findElement(By.xpath("//div[@id='mangaBox']/div[2]/div/div[2]/strong")).getText();
    }

}
