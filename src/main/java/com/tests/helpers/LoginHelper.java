package com.tests.helpers;

import com.tests.ApplicationManager;
import com.tests.base.HelperBase;
import com.tests.models.AccountData;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class LoginHelper extends HelperBase {
    private static final String LOGIN_PAGE_URL = "/internal/auth/login";
    private AccountData accountData;
    private final String loginPageUrl;

    public LoginHelper(ApplicationManager applicationManager) {
        super(applicationManager);
        loginPageUrl = ApplicationManager.BASE_URL + LOGIN_PAGE_URL;
    }

    public void openLoginPage() {
        driver.get(loginPageUrl);
    }

    public void loginUser() {
        if (!isLoggedIn()) {
            openLoginPage();
            driver.findElement(By.id("username")).clear();
            driver.findElement(By.id("username")).click();
            driver.findElement(By.id("username")).sendKeys(accountData.getEmail());
            driver.findElement(By.id("password")).click();
            driver.findElement(By.id("password")).sendKeys(accountData.getPassword());
            driver.findElement(By.xpath("//input[@Type='submit']")).click();
        }
    }

    public String getCurrentEmail() {
        driver.findElement(By.xpath("//div[@id='wrap']/header/div/div/div[2]/div/div/div[6]/a/span")).click();
        driver.findElement(By.xpath("//div[@id='accountMenu']/div/a/div")).click();
        return driver.findElement(By.xpath("//div[@id='mangaBox']/div[3]/div/div/a")).getText();
    }

    public AccountData getAccountData() {
        return accountData;
    }

    public boolean isLoggedIn() {
        try {
            driver.findElement(By.xpath("//div[@id='wrap']/header/div/div/div[2]/div/div/div[6]/a/span"));
            return true;
        } catch (NoSuchElementException exception) {
            return false;
        }
    }

    public boolean isLoggedIn(String username) {
        if (isLoggedIn()) {
            return getCurrentEmail().equals(username);
        } else {
            return false;
        }
    }

    public void setAccountData(AccountData accountData) {
        this.accountData = accountData;
    }

    public void logout() {
        driver.findElement(By.xpath("//div[@id='wrap']/header/div/div/div[2]/div/div/div[6]/a/span")).click();
        driver.findElement(By.xpath("//div[@id='accountMenu']/div/a[6]/div")).click();
    }

    public void logoutIfLogin() {
        if (isLoggedIn()) {
            logout();
        }
    }
}
