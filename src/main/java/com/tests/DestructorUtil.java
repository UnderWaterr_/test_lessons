package com.tests;

public final class DestructorUtil {

    public static void addDestructor(Runnable runnable) {
        Thread hook = new Thread(runnable);
        Runtime.getRuntime().addShutdownHook(hook);
    }

    public static boolean removeDestructor(Thread hook) {
        return Runtime.getRuntime().removeShutdownHook(hook);
    }

    public static void addManagerDestructor(ApplicationManager applicationManager) {
        addDestructor(() -> {
            try {
                applicationManager.getDriver().quit();
            } catch (Exception ignored) {
            }
        });
    }
}
