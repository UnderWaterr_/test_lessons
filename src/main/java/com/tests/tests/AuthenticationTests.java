package com.tests.tests;

import com.tests.base.TestBase;
import com.tests.models.AccountData;
import org.junit.Assert;
import org.junit.Test;

import static com.tests.config.Settings.getAccountDataFromSettings;

public class AuthenticationTests extends TestBase {

    @Test
    public void testSuccessLogin() {
        applicationManager.getLoginHelper().logoutIfLogin();
        applicationManager.getLoginHelper().openLoginPage();
        applicationManager.getLoginHelper().setAccountData(getAccountDataFromSettings());
        applicationManager.getLoginHelper().loginUser();
        Assert.assertTrue(applicationManager.getLoginHelper().isLoggedIn(getAccountDataFromSettings().getName()));
    }

    @Test
    public void testFailedLogin() {
        applicationManager.getLoginHelper().logoutIfLogin();
        AccountData account = new AccountData(TestBase.getRandomString(5),
                TestBase.getRandomString(5),
                TestBase.getRandomString(5));
        applicationManager.getLoginHelper().openLoginPage();
        applicationManager.getLoginHelper().setAccountData(account);
        applicationManager.getLoginHelper().loginUser();
        Assert.assertFalse(applicationManager.getLoginHelper().isLoggedIn(account.getName()));
    }

}
