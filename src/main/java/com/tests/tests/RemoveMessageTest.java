package com.tests.tests;

import com.tests.base.AuthBase;
import org.junit.Assert;
import org.junit.Test;

public class RemoveMessageTest extends AuthBase {

    @Test
    public void testDeleteMessage() throws InterruptedException {
        applicationManager.getDeleteMessageHelper().deleteMessage();
        Assert.assertNotNull(applicationManager.getDeleteMessageHelper().getMessageErrorText());
    }

}
