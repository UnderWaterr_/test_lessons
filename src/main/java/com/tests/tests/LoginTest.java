package com.tests.tests;

import com.tests.base.TestBase;
import org.junit.Assert;
import org.junit.Test;

public class LoginTest extends TestBase {

    @Test
    public void testLogin() {
        applicationManager.getLoginHelper().logoutIfLogin();
        applicationManager.getLoginHelper().loginUser();
        Assert.assertEquals(applicationManager.getLoginHelper().getCurrentEmail(),
                applicationManager.getLoginHelper().getAccountData().getName());
    }

}
