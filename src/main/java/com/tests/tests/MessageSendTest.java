package com.tests.tests;

import com.tests.base.AuthBase;
import com.tests.models.MessageData;
import com.tests.models.jaxb.Messages;
import org.junit.Assert;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

@RunWith(Theories.class)
public class MessageSendTest extends AuthBase {

    @DataPoints
    public static List<MessageData> messagesFromXmlFile() {
        try {
            JAXBContext context = JAXBContext.newInstance(Messages.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Messages messages = (Messages) unmarshaller.unmarshal(new File("src/main/java/resources/" + "/messages.xml"));
            return messages.getMessages();
        } catch (JAXBException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Theory
    public void testMessage(MessageData message) throws Exception {
        applicationManager.getMessageHelper().setMessage(message);
        applicationManager.getMessageHelper().openChatPage();
        applicationManager.getMessageHelper().sendMessage();
        Assert.assertEquals(applicationManager.getMessageHelper().getMessageText(),
                applicationManager.getMessageHelper().getCurrentMessage());
    }
}
