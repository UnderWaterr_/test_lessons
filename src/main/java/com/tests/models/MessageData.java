package com.tests.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Alina Minnibaeva
 */
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageData {

    @XmlElement(name = "recipient", required = true)
    private String recipient;

    @XmlElement(name = "text", required = true)
    private String text;

    public MessageData(String recipient, String text) {
        this.recipient = recipient;
        this.text = text;
    }

    public MessageData() {
    }

    public String getRecipient() {
        return recipient;
    }

    public String getText() {
        return text;
    }
}
