package com.tests.models;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Alina Minnibaeva
 */
@XmlRootElement(name = "account")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountData {
    @XmlElement(name = "email", required = true)
    private String email;

    @XmlElement(name = "password", required = true)
    private String password;

    @XmlElement(name = "name", required = true)
    private String name;

    public AccountData(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public AccountData() {
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}