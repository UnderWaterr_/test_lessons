package com.tests.generator;

import com.tests.base.TestBase;
import com.tests.models.MessageData;
import com.tests.models.jaxb.Messages;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Generator {
    public static final String DIRECTORY = "src/main/java/resources/";

    //message 3 messages xml
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            String[] values = scanner.nextLine().split(" ");
            String type = values[0];
            int count = Integer.parseInt(values[1]);
            String filename = values[2];
            String format = values[3];
            if (Objects.equals(type, "message")) {
                GenerateForGroups(count, filename, format);
            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    private static void GenerateForGroups(int count, String filename, String format) {
        List<MessageData> messages = new LinkedList<>();
        for (int i = 0; i < count; i++) {
            messages.add(new MessageData(
                    "M.Necromancer",
                    TestBase.getRandomString(5)
            ));
        }
        if (Objects.equals(format, "xml")) {
            try (FileWriter fileWriter = new FileWriter(DIRECTORY + "/" + filename + "." + format)) {
                writePostsToXmlFile(messages, fileWriter);
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    static void writePostsToXmlFile(List<MessageData> messageDataList, FileWriter fileWriter) {
        try {
            Messages messages = new Messages();
            messages.setMessages(messageDataList);
            JAXBContext jaxbContext = JAXBContext.newInstance(Messages.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(messages, fileWriter);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
