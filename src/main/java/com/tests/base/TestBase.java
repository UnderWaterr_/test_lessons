package com.tests.base;

import com.tests.ApplicationManager;
import org.junit.Before;

/**
 * @author Alina Minnibaeva
 */

public class TestBase {
    protected ApplicationManager applicationManager;

    @Before
    public void setUp() {
        applicationManager = ApplicationManager.getInstance();
        applicationManager.counter++;
    }

    public static String getRandomString(int length) {
        String alphabet = "abcdefghijklmnopqrstuvwqyz1234567890";
        char[] chars = alphabet.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        for (int j = 0; j < length; j++) {
            stringBuilder.append(chars[(int) Math.round(Math.random() * (chars.length - 1))]);
        }
        return stringBuilder.toString().trim();
    }
}
