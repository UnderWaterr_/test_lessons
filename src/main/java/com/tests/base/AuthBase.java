package com.tests.base;

import org.junit.Before;

import static com.tests.config.Settings.getAccountDataFromSettings;

public class AuthBase extends TestBase {

    @Before
    public void setUp() {
        super.setUp();
        applicationManager.getLoginHelper().setAccountData(getAccountDataFromSettings());
        applicationManager.getLoginHelper().loginUser();
    }

}
