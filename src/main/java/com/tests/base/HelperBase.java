package com.tests.base;

import com.tests.ApplicationManager;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class HelperBase {
    protected WebDriver driver;
    protected ApplicationManager applicationManager;

    public HelperBase(ApplicationManager applicationManager) {
        this.applicationManager = applicationManager;
        this.driver = applicationManager.getDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
    }
}
